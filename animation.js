/*jshint esversion: 7 */
'esversion: 7';

let canvas = document.querySelector('canvas');
let c = canvas.getContext('2d');
// c.rotate(Math.PI / 4);
let gravity = 0.3;
let bounce = 0.8;
let wallBounce = 1;
let mouseX = 0;
let mouseY = 0;

onmousemove = function (e) {
  // console.log("mouse location:", e.clientX, e.clientY);
  mouseX = e.clientX;
  mouseY = e.clientY;
};

function Ball(x, y, dx, dy, radius, mass, color, gravity=true) {
  let self = {
    x: x,
    y: y,
    dx: dx,
    dy: dy,
    radius: radius,
    mass: mass,
    color: color,
    gravity: gravity
  };

  self.update = () => {
    self.draw();

    //console.log("location: ", self.x, self.y);
    if (self.y + self.radius + self.dy > canvas.height) {
      self.dy = -self.dy * bounce;
    } else if ((self.y + self.radius + self.dy + 100 > mouseY) && (Math.abs(self.x - (mouseX - 200)) < self.radius)) {
      self.dy = -self.dy * bounce;
    } else if (self.gravity) {
      self.dy += gravity;
    }

    if (self.x + self.radius + self.dx >= canvas.width) {
      self.dx = -self.dx - wallBounce;
    }
    if (self.x - self.radius <= 0) {
      self.dx = -self.dx + wallBounce;
    }
    self.x += self.dx;
    self.y += self.dy;

  };

  self.draw = () => {
    c.beginPath();
    c.arc(self.x, self.y, self.radius, 0, Math.PI * 2, false);
    c.fillStyle = self.color;
    c.fill();
    // c.stroke();
    c.closePath();

    // velocity vector
    c.beginPath();
    c.moveTo(self.x, self.y);
    c.lineTo(self.x + self.dx*10, self.y + self.dy*10);
    c.lineWidth = 5;
    c.strokeStyle = 'black';
    c.stroke();
  };

  return self;
}



Ball.updateVelocities = (b1, b2) => {
  let dx = b2.x - b1.x;
  let dy = b2.y - b1.y;
  let hyp = Math.sqrt(dx * dx + dy * dy);

  console.log(`b1: (${b1.x}, ${b1.y} delta ${b1.dx}, ${b1.dy})           b2: (${b2.x}, ${b2.y} delta ${b2.dx}, ${b2.dy})      deltas: ${dx}, ${dy}`);

  if (hyp > b1.radius + b2.radius) return;

  // original velocity vector
  c.strokeStyle = '#00aa00';
  c.lineWidth = 3;
  
  c.beginPath();
  c.moveTo(b1.x, b1.y);
  c.lineTo(b1.x + b1.dx*10, b1.y + b2.dy*10);
  c.stroke();

  c.beginPath();
  c.moveTo(b2.x, b2.y);
  c.lineTo(b2.x + b2.dx*10, b2.y + b2.dy*10);
  c.stroke();

  // // delta velocity vector
  // c.strokeStyle = "#00ff00";
  // c.lineWidth = 3;

  // c.beginPath();
  // c.moveTo(b1.x, b1.y);
  // c.lineTo(b1.x - b2.dx * d1*10, b1.y - b2.dy * d1*10);
  // c.stroke();

  // c.beginPath();
  // c.moveTo(b2.x, b2.y);
  // c.lineTo(b2.x + b1.dx * d2*10, b2.y + b1.dy * d2*10);
  // c.stroke();

  let v0x1 = b1.dx;
  let v0y1 = b1.dy;
  let v0x2 = b2.dx;
  let v0y2 = b2.dy;

  let algebra = (m1, v1, m2, v2) => (2*m1*v1 + v2*(m2-m1)) / (m1 + m2);

  b1.dx = algebra(b1.mass, v0x1, b2.mass, v0x2);
  b1.dy = algebra(b1.mass, v0y1, b2.mass, v0y2);
  b2.dx = algebra(b2.mass, v0x2, b1.mass, v0x1);
  b2.dy = algebra(b2.mass, v0y2, b1.mass, v0y2);

  // console.log(`momentum: ${Math.sqrt(bx1**2 + by1**2) * Math.sqrt(bx2**2 + by2**2)} -> ${Math.sqrt(b1.dx**2 + b1.dy**2) * Math.sqrt(b2.dx**2 + b2.dy**2)}`);

  console.log(`new velocities: b1: (${algebra(b1.mass, v0x1, b2.mass, v0x2)}, ${b1.dy})           b2: (${b2.dx}, ${b2.dy})`);

};



let ball;
let ballArray = [];

function init() {
  let radius = 30;
  for (var i = 0; i < 2; i++) {
    let x = Math.floor(Math.random() * (canvas.width - radius - 1) + radius);
    let y = Math.floor(Math.random() * (canvas.height - radius - 1) + 0) - 600;
    let dx = Math.floor(Math.random() * (-2 - 1) + 2);
    let dy = Math.floor(Math.random() * (-2 - 1) + 2);
    ballArray.push(Ball(x, y, dx / 10, dy / 10, radius, radius, '#8404DA'));
  }
  // ball = new Ball(canvas.width / 2, canvas.height / 2, 2, 30, 'red');
}


let todo_delete_count = 0;

function animate() {

  c.clearRect(0, 0, canvas.width, canvas.height);

  for (let i = 0; i < ballArray.length; i++) {
    for (let j = i + 1; j < ballArray.length; j++) {
      Ball.updateVelocities(ballArray[i], ballArray[j]);
    }
  }

  for (let ball of ballArray) {
    ball.update();
  }

  todo_delete_count += 1;
  console.log(`frame ${todo_delete_count} finished.`);
  if (todo_delete_count > 12) return;
  // alert();
  setTimeout(() => requestAnimationFrame(animate), 0);
}

// init();
ballArray = [
  Ball(350, 150, 4, 0, 30, 30, '#ff0000'),
  Ball(350, 300, 4, 0, 30, 30, '#00ff00', false)
  // Ball(550, 150, -2, 0, 30, '#0000ff')
];
// animate();
document.getElementById("step-button").addEventListener("click", animate);

//
//
// document.onkeypress = function (e) {
//     e = e || window.event;
//     if (e.keyCode == 113){
//         alert("yes")
//     }
//
// };
let canvas = document.querySelector('canvas');
let c = canvas.getContext('2d');
// c.rotate(Math.PI / 4);
let gravity = 0.6;
let bounce = 0.8;
let wallBounce = 0.1;
let mouseBounce = 0.8;
let mouseX = 0;
let mouseY = 0;

onmousemove = function (e) {
  // console.log("mouse location:", e.clientX, e.clientY);
  mouseX = e.clientX;
  mouseY = e.clientY;
}

function Ball(x, y, dx, dy, radius, color) {
  let self = {
    x: x,
    y: y,
    dx: dx,
    dy: dy,
    radius: radius,
    color: color
  };



  self.update = () => {
    //console.log("location: ", self.x, self.y);
    if (self.y + self.radius + self.dy > canvas.height) {
      self.dy = -self.dy * bounce;
    } else if ((self.y + self.radius + self.dy + 100 > mouseY) && (Math.abs(self.x - (mouseX - 200)) < self.radius)) {
      self.dy = -self.dy * mouseBounce;
    } else {
      self.dy += gravity;
    }
    if (self.x + self.radius + self.dx >= canvas.width) {
      self.dx = -self.dx - wallBounce;
    }
    if (self.x - self.radius <= 0) {
      self.dx = -self.dx + wallBounce;
    }
    self.x += self.dx;
    self.y += self.dy;
    self.draw();
  }

  self.draw = () => {
    c.beginPath();
    c.arc(self.x, self.y, self.radius, 0, Math.PI * 2, false)
    c.fillStyle = self.color;
    c.fill();
    // c.stroke();
    c.closePath();

    // velocity vector
    c.beginPath();
    c.moveTo(self.x, self.y);
    c.lineTo(self.x + self.dx * 10, self.y + self.dy * 10);
    c.stroke();
  }

  self.clicker = () => {
    // self.dy -=10;
    // antigrav = true;
    if (self.x + self.radius + self.dx > (mouseX - 200)) {
      self.dx += 10;


    } else {
      self.dx -= 10;

    }
    console.log(gravity);

  }
  document.addEventListener("click", self.clicker);
  return self;
}

function antigrav() {
  gravity = -gravity

}

document.onkeypress = function (e) {
  e = e || window.event;
  if (e.keyCode == 103) {
    gravity = -gravity
  }
};



Ball.updateVelocities = (b1, b2) => {
  let dx = b2.x - b1.x;
  let dy = b2.y - b1.y;
  let hyp = Math.sqrt(dx * dx + dy * dy);

  console.log(`b1: (${b1.x}, ${b1.y} delta ${b1.dx}, ${b1.dy})           b2: (${b2.x}, ${b2.y} delta ${b2.dx}, ${b2.dy})      deltas: ${dx}, ${dy}`);

  if (hyp > b1.radius + b2.radius) return;

  let d1 = (b2.dx * dx + b2.dy * dy); // amount of change (dot product of b2 velocity on <dx, dy>)
  let d2 = (b1.dx * dx + b1.dy * dy);

  let hyp1 = Math.sqrt(b1.dx * b1.dx + b1.dy * b1.dy);
  let hyp2 = Math.sqrt(b2.dx * b2.dx + b2.dy * b2.dy);

  let bx1 = b1.dx;
  let by1 = b1.dy;
  b1.dx -= b2.dx * d1 / hyp1;
  b1.dy -= b2.dy * d1 / hyp1;
  b2.dx += bx1 * d2 / hyp2;
  b2.dy += by1 * d2 / hyp2;

  console.log(`new velocities: b1: (${b1.dx}, ${b1.dy})           b2: (${b2.dx}, ${b2.dy})`);

}


let ball;
let ballArray = [];

function init() {
  let radius = 30;
  for (var i = 0; i < 9; i++) {
    let x = Math.floor(Math.random() * (canvas.width - radius - 1) + radius);
    let y = Math.floor(Math.random() * (canvas.height - radius - 1) + 0) - 600;
    let dx = Math.floor(Math.random() * (-2 - 1) + 2);
    let dy = Math.floor(Math.random() * (-2 - 1) + 2);
    ballArray.push(Ball(x, y, dx / 10, dy / 10, radius, '#8404DA'));
    console.log(ballArray[ballArray.length - 1])
  }
  // ball = new Ball(canvas.width / 2, canvas.height / 2, 2, 30, 'red');
}

function animate() {
  requestAnimationFrame(animate)

  c.clearRect(0, 0, canvas.width, canvas.height);

  for (let i = 0; i < ballArray.length; i++) {
    ballArray[i].update()
    for (let j = i + 1; j < ballArray.length; j++) {
      Ball.updateVelocities(ballArray[i], ballArray[j]);
    }
  }


}


init();
animate();
